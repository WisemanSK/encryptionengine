//**********************************************************************************************************************
//
//          File    : EncryptionEngine.java
//          Class   : EncryptionEngine
//          Author  : Wiseman Abonile Sicuku
//          Date    : 2020-01-24
//          Description : This class defines the entry point of the Encryption Engine Software - MainApp
//
//  W.A Sicuku
//**********************************************************************************************************************

/**
 *
 * Hardware Security Modules
 * ---------------------------------------------------------------------------------------------------------------------
 * A hardware security module (HSM)is a physical computing device that safeguards and manages digital keys for strong
 * authentication and provides cryptoprocessing.These modules traditionally come in the form of a plug-in card or an
 * external device that attaches directly to a computer or network server.A hardware security module contains one or
 * more secure cryptoprocessor.
 *
 * Reference:   https://en.wikipedia.org/wiki/Hardware_security_module
 *
 *
 * STS Transactional Hardware Security Modules
 * ---------------------------------------------------------------------------------------------------------------------
 * TSM-250:
 *
 * This a TSM250 USB security module only,and is used for STS vending(supports both base dates 1993and 2014).
 * The Vending System must be designed in accordance with IEC 62055-41and 62055-51Ed3,and must be tested in
 * accordance with the STS compliance test specifications for Vending Systems STS 531-1part 4.Prism supplies the
 * security module and the STS600 API Specification(now an STS Standard).An optional High-Level Software Layer will
 * be available later this year.
 *
 * TSM-500:
 *
 * This a TSM500i NSS security module only,and is used for STS vending(supports both base dates 1993and 2014).
 * The Vending System must be designed in accordance with IEC 62055-41and 62055-51Ed3,and must be tested in
 * accordance with the STS compliance test specifications for Vending Systems STS 531-1part 4.Prism supplies the
 * security module and the STS600 API Specification(now an STS Standard).
 *
 * Reference:   https://www.prism.co.za/sts-security-modules/
 *
 *
 *  Encryption Engine - Architecture
 *----------------------------------------------------------------------------------------------------------------------
 *
 *
 *                            +++++++++++++++++++
 *                            +                 +
 *                            +   E-Engine      +
 *                            +                 +
 *                            +++++++++++++++++++
 *                                     |
 *                            +++++++++++++++++++
 *                            +                 +
 *                            +    AI-Engine    +
 *                            +                 +
 *                            +++++++++++++++++++
 *                                     |
 *  ++++++++++++++            +++++++++++++++++++                 ++++++++++++++++++
 *  +            +            +                 +                 +                +
 *  +  CTS-GEN   +            +     STS-GEN     +                 +   TMT-GEN      +
 *  +            +            +                 +                 +                +
 *  ++++++++++++++            +++++++++++++++++++                 ++++++++++++++++++
 *         |                            |                                |
 *         ---------------------------  |  -------------------------------
 *                                    | | |
 *                           ++++++++++++++++++++++                   +++++++++++++++++
 *                           +                    +                   +               +
 *                           +    E-SOCKET        +-------------------+  S-CARD RD    +---------------------
 *                           +                    +                   +               +                    |
 *                           ++++++++++++++++++++++                   +++++++++++++++++                    |
 *                                   |  | |                                                                |
 *       -----------------------------  | -------------------------------                                  |
 *       |                              |                               |                                  |
 *       |                              |                               |                                  |
 *       |                              |                               |                                  |
 *       |                              |                               |                                  |
 *       |                              |                               |                                  |
 * ++++++++++++++               +++++++++++++++++               ++++++++++++++++                    ++++++++++++++++
 * +            +               +               +               +              +                    +              +
 * +    STS     +               +     TMT       +               +     CTS      +                    +    CSE       +
 * +            +               +               +               +              +                    +              +
 * ++++++++++++++               +++++++++++++++++               ++++++++++++++++                    ++++++++++++++++
 *
 *
 * ---------------------------------------------------------------------------------------------------------------------
 */

public class MainApp {

    public static void main(String[] args) {

    }
}
