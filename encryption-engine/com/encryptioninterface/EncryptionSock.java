package com.encryptioninterface;
//**********************************************************************************************************************
//
//          File    : EncryptionSocket.java
//          Class   : EncryptionSocket
//          Author  : Wiseman Abonile Sicuku
//          Date    : 2020-01-20
//          Description : This class encapsulate all the functionality required by Encryption Servers in order to communi-
//                      : cate with Hardware Security Modules (HSM). The class supports all the STS-Modules, CTS-Module
//                      : and the Tellumat Encryption Card.
// W.A Sicuku
//**********************************************************************************************************************
import com.encryptionprotocol.EncryptionTokenRequest;

import java.util.*;
import java.net.*;
import java.io.*;

public class EncryptionSock extends Thread implements EncryptionSockUtil {
    // Private Fields
    //------------------------------------------------------------------------------------------------------------------

    private String pinCSE;
    private int portNumber;
    private int retryCount;
    private String ipAddress;
    private long masterIDCSE;
    private Socket connSocket;
    private boolean isConnected;
    private String requestMessage;
    private String responseMessage;
    private boolean isErrorCritical;
    private boolean tellumatSecureF;
    private DataInputStream encryptionHandleIn;
    private EncryptionCardReader connectCSECard;
    private DataOutputStream encryptionHandleOut;
    private EncryptionAlgorithm algorithm = new EncryptionAlgorithm();
    private Map< String,Integer> registerMap = new HashMap< String,Integer>();
    // Constructor
    //------------------------------------------------------------------------------------------------------------------

    EncryptionSock(String ipAddress, int portNumber, int retryCount, boolean isConnected) {
        this.isConnected = isConnected;
        this.tellumatSecureF = false;
        this.isErrorCritical = false;
        this.portNumber = portNumber;
        this.retryCount = retryCount;
        this.ipAddress = ipAddress;
    }

    //******************************************************************************************************************
    //
    //          Name    : encryptionConnect ()
    //          Description : This method opens the serial port and TCP/IP socket in order to connect the application to the
    //                      : Hardware Security Module (only those who supports serial communication and internet).
    //
    //******************************************************************************************************************
    public void encryptionConnect(String portName, EncryptionServerTypes serverType, String pinCSE, long masterIDCSE) {
        this.masterIDCSE = masterIDCSE;
        this.pinCSE = pinCSE;
        if (serverType == serverType.CTS) {
            connectCSECard = new EncryptionCardReader(portName);
            if (connectCSECard != null) {
                try {
                    connectCSECard.access();
                } catch (RuntimeException eErr) {
                    System.out.println("EncryptionSocket-encryptionConnect : Failed to connect to CSE Encryption card.");
                }
            }
        } else {
            if (connSocket == null) {
                while (!isConnected) {
                    try {
                        connSocket = new Socket(ipAddress, portNumber);
                        System.out.println("Just connected to " + connSocket.getRemoteSocketAddress() + " : " +
                                connSocket.getPort());
                    } catch (SocketTimeoutException eErr) {
                        System.out.println("EncryptionSocket-encryptionConnect : Socket timed out!");
                        break;
                    } catch (IOException eErr) {
                        eErr.printStackTrace();
                        break;
                    }
                }
            }
        }
    }




    //******************************************************************************************************************
    //
    //          Name    : readResponseMessage ()
    //          Description : This method reads response message from the server (Hardware Security Modules - Servers)
    //                      : and stores it.
    //
    //******************************************************************************************************************
    public void readResponseMessage() throws IOException {
        isErrorCritical = false;
        if (connSocket != null) {
            encryptionHandleIn = new DataInputStream(connSocket.getInputStream());
            try {   //-------------
                responseMessage = encryptionHandleIn.readUTF();
            } catch (IOException eErr) {
                System.out.println("Encryption-readResponseMessage : Failed to read data from the sender.");
                isErrorCritical = true;
            }
        }
    }



    //******************************************************************************************************************
    //
    //          Name    : sendRequestMessage ()
    //          Description : This method sends the request message to the server (Hardware Security Modules - Servers)
    //                      : and wait for the response.
    //
    //******************************************************************************************************************
    public void sendRequestMessage(String message) throws IOException {
        isErrorCritical = false;
        if (connSocket != null) {
            encryptionHandleOut = new DataOutputStream(connSocket.getOutputStream());
            try { //--------
                encryptionHandleOut.writeUTF(message);
            } catch (IOException eErr) {
                System.out.println("Encryption-sendRequestMessage : Failed to send data to the receiver.");
                isErrorCritical = true;
            }
        }
    }



    //******************************************************************************************************************
    //
    //          Name    : generateEncryptedToken ()
    //          Description : This method generates STS, CTS and TMT token. Server (Hardware Security Modules - Servers)
    //                      : type must be provided in order to generate a valid token.
    //
    //******************************************************************************************************************
    public boolean generateEncryptedToken(EncryptionTokenRequest tokenRequest, EncryptionServerTypes serverType, int keyRevision) throws IOException {
        String supplyGroupString;
        long cseMeterNumber = 0;
        int keyRegister = 0;
        switch (serverType) {
            case TMT:
                if (!generateTMTToken(tokenRequest)) {
                    return false;
                }
                break;
            case STS:
                supplyGroupString = tokenRequest.supplyGroup;
                if (!getSTSKeyRegister(tokenRequest.supplyGroup)) {
                    System.out.println("EncryptionSocket-generateEncryptedToken : Incorrect key-register for supply group  " +
                            supplyGroupString + ".");
                    return false;
                }
                if (!generateSTSToken(tokenRequest, keyRegister, keyRevision)) {
                    return false;
                }
                break;
            case CTS:
                if (!generateCSEToken(tokenRequest)) {
                    return false;
                }
                break;
            default:
                System.out.println("EncryptionSocket-generateEncryptedToken : Server (Hardware Security Module) not supported.");

        }
        return true;
    }



    //******************************************************************************************************************
    //
    //          Name    : generateSTSToken ()
    //          Description : This method generates STS token. Request string (structure) will all its data must be
    //                      : provided.
    //
    //******************************************************************************************************************
    public boolean generateSTSToken(EncryptionTokenRequest tokenRequest, int keyRegister, int keyRevision) throws IOException {
        char[] PAN;
        String supplyGroup;
        String meterNumber;
        char luhnCharecter;
        String typeSTSRequest;
        String oldSupplyGroup = null;
        String typeSTSResponse;

        int oldKeyRegister = 0;
        meterNumber = tokenRequest.meterNumber;
        supplyGroup = tokenRequest.supplyGroup;
        if (tokenRequest.algorithm != STS_ALGORITHM) {
            System.out.println("EncryptionSocket-generateSTSToken : Algorithm not supported.");
            return false;
        }

        if (tokenRequest.tokenClass != STS_CLASS_MAN_SET_DISP_KEY_A ) {
            oldSupplyGroup = tokenRequest.oldSupplyGroup;
            if (!getSTSKeyRegister(tokenRequest.oldSupplyGroup)){
                System.out.println("EncryptionSocket-generateSTSToken : Incorrect supply group");
                return false;
            }
        }

        switch (tokenRequest.tokenClass) {
            case STS_CLASS_CREDIT_TRANSFER_ELEC:
            case STS_CLASS_CREDIT_TRANSFER_WATER:
            case STS_CLASS_CREDIT_TRANSFER_GAS: {
                typeSTSRequest = GENERATE_STS_CREDIT_TOKEN_REQ;
                typeSTSResponse = GENERATE_STS_CREDIT_TOKEN_RESP;
            }
            break;

            case STS_CLASS_MAN_SET_MAX_PWR_LOAD: {
                tokenRequest.tokenData = convertTransferAmount((convertTransferAmountBack(tokenRequest.tokenData) * 100));
                typeSTSRequest = GENERATE_STS_MAN_REQ;
                typeSTSResponse = GENERATE_STS_CREDIT_TOKEN_RESP;
            }
            break;

            case STS_CLASS_MAN_CLEAR_CREDIT: {
                typeSTSRequest = GENERATE_STS_MAN_REQ;
                typeSTSResponse = GENERATE_STS_CREDIT_TOKEN_RESP;
                tokenRequest.tokenData = 0;
                if (tokenRequest.hdr.utility == WATER_PREPAYMENT) {
                    tokenRequest.tokenData = 0x01;
                }

                if (tokenRequest.hdr.utility == GAS_PREPAYMENT) {
                    tokenRequest.tokenData = 0xFFFF;
                }
            }
            break;

            case STS_CLASS_MAN_CLEAR_TAMPER_CONDITION: {
                tokenRequest.tokenData = 0;
                typeSTSRequest = GENERATE_STS_MAN_REQ;
                typeSTSResponse = GENERATE_STS_MAN_RESP;
            }
            break;

            case STS_CLASS_MAN_SET_DISP_KEY_A: {
                if (tokenRequest.hdr.pkt_vers < 2) {
                    System.out.println("EncryptionSocket-generateSTSToken : Invalid packet version for this request.");
                    return false;
                }
                tokenRequest.tokenData = 0;
                typeSTSRequest = GENERATE_STS_KEY_CHANGE_REQ;
                typeSTSResponse = GENERATE_STS_KEY_CHANGE_RESP;
            }
            break;

            default:
                System.out.println("EncryptionSocket-generateSTSToken : Token class not supported.");
                return false;

        }

        PAN = (STS_ELECTRICITY_ISOBIN_NO + meterNumber).toCharArray();
        luhnCharecter = algorithm.checkLuhn(PAN, 17);
        if (luhnCharecter == 'Z') {
            System.out.println("EncryptionSocket-generateSTSToken : PAN Luhn check failed.");
            return false;
        }

        if (tokenRequest.tokenClass == STS_CLASS_MAN_SET_DISP_KEY_A) {   //key change request
            requestMessage = typeSTSRequest +
                    PAN +
                    oldKeyRegister +
                    keyRegister +
                    oldSupplyGroup +
                    supplyGroup +
                    tokenRequest.oldTariffIndex +
                    tokenRequest.tariffIndex +
                    keyRevision +
                    keyRevision +
                    KEY_EXPIRY_NUMBER +
                    KEY_EXPIRY_NUMBER;


            requestMessage.toUpperCase();
            long crc = algorithm.addCRCToMessage(requestMessage);
            requestMessage += crc;
            if (requestMessage.length() == 200) {
                sendRequestMessage(requestMessage);
                readResponseMessage();
                if (responseMessage.length() != 250) {
                    System.out.println("EncryptionSocket-generateSTSToken : Invalid STS response.");
                    isErrorCritical = true;
                    return false;
                }
            }
            convertResponseToToken(responseMessage);
            return true;

        }

        responseMessage = typeSTSRequest +
                PAN +
                keyRegister +
                supplyGroup +
                tokenRequest.tariffIndex +
                keyRevision +
                KEY_EXPIRY_NUMBER +
                tokenRequest.tokenClass+
                tokenRequest.tokenID +
                tokenRequest.tokenData;


        requestMessage.toUpperCase();
        long crc = algorithm.addCRCToMessage(requestMessage);
        requestMessage += crc;
        if (requestMessage.length() == 200) {
            sendRequestMessage(requestMessage);
            readResponseMessage();
            if (!validateSTSResponse(responseMessage.length(), GENERATE_STS_CREDIT_TOKEN_RESP)) {
                System.out.println("EncryptionSocket-generateSTSToken : Invalid STS response.");
                isErrorCritical = true;
            }
        }
        convertResponseToToken(responseMessage);

        return true;
    }



    //******************************************************************************************************************
    //
    //          Name    : generateTMTToken ()
    //          Description : This method generates TMT token. Request string (structure) will all its data must be
    //                      : provided.
    //
    //******************************************************************************************************************
    public boolean generateTMTToken(EncryptionTokenRequest tokenRequest) throws IOException {
        String pc;
        String tokenData;
        String outPutStr;
        String tempToken = null;
        String serialData;
        String randomNibble;
        String meterNumberStr;
        String subclassNibble;
        String supplyGroupsStr;
        isErrorCritical = false;

        long tokenNumber = tokenRequest.tokenData;
        supplyGroupsStr = tokenRequest.supplyGroup;
        if (tokenRequest.algorithm != PLESSEY_ALGORITHM) {
            System.out.println("EncryptionSocket-generateTMTToken : Algorithm not supported.");
            return false;
        }

        int temptype = 0;
        switch (temptype) {
            case METER_TYPE_SEQUENCE_WHOLE_UNIT:
            case METER_TYPE_SEQUENCE_TENTH_UNIT:
                if (tokenRequest.tokenID.equals("8000")) {
                    System.out.println("EncryptionSocket-generateTMTToken : Sequence number data out of range.");
                    return false;
                }

                outPutStr = tokenRequest.meterNumber;
                if (tokenRequest.meterType == METER_TYPE_SEQUENCE_TENTH_UNIT) {
                    meterNumberStr = outPutStr.substring(10,15);
                }
                serialData = outPutStr.substring(1,2);

                switch (temptype) {
                    case STS_CLASS_CREDIT_TRANSFER_ELEC:
                        if (tokenRequest.meterType == METER_TYPE_SEQUENCE_WHOLE_UNIT)
                            tokenNumber /= 10;
                        break;

                    case STS_CLASS_MAN_CLEAR_TAMPER_CONDITION:
                        if (tokenRequest.meterType == METER_TYPE_SEQUENCE_WHOLE_UNIT) {
                            tokenRequest.tokenID = "1";
                            tokenNumber = 0;
                        } else {
                            tokenNumber = 9061;
                        }
                        break;

                    case PLESSEY_CLASS_MAN_SET_CURRENT_LIMIT:
                        tokenNumber /= 10;
                        if ((tokenNumber > 59)) {
                            System.out.println("EncryptionSocket-generateTMTToken : Invalid Amps limit requested (>59).");
                            return false;
                        }

                        if (tokenRequest.meterType == METER_TYPE_SEQUENCE_WHOLE_UNIT) {
                            tokenNumber = 900 + tokenNumber;
                        } else {
                            tokenNumber = 9000 + tokenNumber;
                        }
                        break;

                    case STS_CLASS_MAN_CLEAR_CREDIT:
                        tokenRequest.tokenID = "2";
                        tokenNumber = 0;
                        break;

                    case PLESSEY_CLASS_MAN_CLEAR_REVERSE_FLAG:
                        tokenRequest.tokenID = "3";
                        tokenNumber = 0;
                        break;

                    case PLESSEY_CLASS_MAN_DISPLAY_PREVIOUS_BALANCE:
                        tokenRequest.tokenID = "6";
                        tokenNumber = 0;
                        break;

                    default:
                        System.out.println("EncryptionSocket-generateTMTToken : Invalid token class.");
                        return false;

                }
                tokenData = tokenRequest.tokenID + tokenNumber;
                break;

            case METER_TYPE_PTS_SINGLE_PHASE:
            case METER_TYPE_PTS_THREE_PHASE:
                meterNumberStr = tokenRequest.meterNumber;
                serialData = meterNumberStr.substring(2,7);

                switch (tokenRequest.tokenClass) {
                    // Non-electricity credit requests not yet supported...
                    case STS_CLASS_CREDIT_TRANSFER_WATER:
                    case STS_CLASS_CREDIT_TRANSFER_GAS:
                    case STS_CLASS_CREDIT_TRANSFER_CONNECTION_TIME:
                    case STS_CLASS_CREDIT_TRANSFER_CURRENCY:
                        // Some specific management functions are not yet supported &&&
                    case STS_CLASS_MAN_SET_TARIFF_RATE:
                    case STS_CLASS_MAN_SET_DISP_KEY_A:
                    case STS_CLASS_MAN_SET_DISP_KEY_B:
                    case STS_CLASS_MAN_SET_PHASE_PWR_UNBALANCE_LIMIT:
                    case STS_CLASS_MAN_SET_WATER_FACTOR:
                    case STS_CLASS_MAN_VERIFY_DISPENSER_KEY:
                        System.out.println("EncryptionSocket-generateTMTToken : Token class not supported");
                        return false;
                    case STS_CLASS_CREDIT_TRANSFER_ELEC:
                        break;

                    case STS_CLASS_MAN_SET_MAX_PWR_LOAD:
                        tokenRequest.tokenData = convertTransferAmount(convertTransferAmountBack(tokenRequest.tokenData));
                        break;

                    case STS_CLASS_MAN_CLEAR_CREDIT:
                        tokenRequest.tokenID = "1";
                        tokenRequest.tokenData = 0;
                        break;

                    case STS_CLASS_MAN_CLEAR_TAMPER_CONDITION:
                        tokenRequest.tokenID = "2";
                        tokenRequest.tokenData = 0;
                        break;

                    default:
                        System.out.println("EncryptionSocket-generateTMTToken : Token class not supported.");
                        return false;

                }

                tokenData = tokenRequest.tokenClass + "0x2FF";
                break;

            default:
                System.out.println("EncryptionSocket-generateTMTToken : Error generating TMT token");
                return  false;

        }

        requestMessage = serialData;
        if (requestMessage.length() == 1500){
            sendRequestMessage(requestMessage);
            readResponseMessage();
            if (responseMessage.length() != 250) {
                System.out.println("EncryptionSocket-generateTMTToken : Invalid TMT response.");
                isErrorCritical = true;
                return false;
            }

        }

        if (tokenRequest.meterType == METER_TYPE_SEQUENCE_WHOLE_UNIT || tokenRequest.meterType == METER_TYPE_SEQUENCE_TENTH_UNIT){
            requestMessage = "!" + tokenData + "0x0d" + "0";
            if (requestMessage.length() == 100) {
                sendRequestMessage(requestMessage);
                readResponseMessage();
                if (responseMessage.charAt(6)!= 'N') {
                    System.out.println("EncryptionSocket-generateTMTToken : Invalid TMT response.");
                    isErrorCritical = true;
                    return false;
                }
            }
        }

        switch (tokenRequest.meterType){
            case METER_TYPE_SEQUENCE_WHOLE_UNIT:
            case METER_TYPE_SEQUENCE_TENTH_UNIT:
                if (!convertResponseToSEQToken(tempToken))
                    return false;
                responseMessage = tempToken;
                break;

            case METER_TYPE_PTS_SINGLE_PHASE:
            case METER_TYPE_PTS_THREE_PHASE:
                if (responseMessage.charAt(6) == 'D'){
                    System.out.println("EncryptionSocket-generateTMTToken : Error encrypting TMT token.");
                }

                if (!convertResponseToPTSToken(tempToken))
                    return false;
                responseMessage = tempToken;
                break;
            default:
                System.out.println("EncryptionSocket-generateTMTToken : Error generating TMT token.");
                return false;

        }

        return true;
    }


    //******************************************************************************************************************
    //
    //          Name    : generateCSEToken ()
    //          Description : This method generates CSE token. Request string (structure) will all its data must be
    //                      : provided.
    //
    //******************************************************************************************************************
    public boolean generateCSEToken(EncryptionTokenRequest tokenRequest) throws IOException {
        long cseTokenID = 0;
        String cseUserPin;
        long cseTransferAmount;
        boolean newFirmware = false;

        if (tokenRequest.meterNumber.length() >= 900000)
            newFirmware = true;

        long masterIDNumber = (masterIDCSE << 8);
        tokenRequest.tokenID = "0x000007FF";
        cseUserPin = pinCSE + "0x0000070FF";

        for (int i = 0; i <=5; i++) {
            cseUserPin += "00FFA0004";
        }

        switch (tokenRequest.tokenClass){
            case TOKEN_TYPE_CREDIT:
                if (newFirmware && tokenRequest.meterType == METER_TYPE_SEQUENCE_WHOLE_UNIT){
                    cseTransferAmount = 0x40;
                }
                break;

            case TOKEN_TYPE_TAMPER_RESET: break;
            case TOKEN_TYPE_SET_OVERLOAD:
                //(index) Level (binary equivalent) Overload Limit
                //	00	0000	disable
                //	01	0001	5   A
                //	02	0010	10  A
                //	03	0011	15  A
                //	04	0100	20  A
                //	05	0101	25  A
                //	06	0110	30  A
                //	07	0111	35  A
                //	08	1000	40  A
                //	09	1001	45  A
                //	10	1010	50  A
                //	11	1011	55  A
                //	12	1100	60  A
                //	13	1101	65  A
                //	14	1110	70  A
                //	15	1111	100 A

                if (newFirmware) {
                    tokenRequest.tokenData = tokenRequest.tokenData + 0x0FF;
                    cseTransferAmount = tokenRequest.tokenData;
                }else {
                    cseTransferAmount = 0x0A;
                }
                break;

            case TOKEN_TYPE_CLEAR_CREDIT: break;
            case TOKEN_TYPE_CLEAR_LOG: break;
            case TOKEN_TYPE_SET_LED_LEVELS: {
                byte[] ledThresholdLevel = new byte[2];
                ledThresholdLevel[1] = 23;
                ledThresholdLevel[0] = 23;
                if (ledThresholdLevel[1] > ledThresholdLevel[0]) {
                    return false;
                }
                cseTransferAmount = tokenRequest.tokenData;
                break;
            }
            case TOKEN_TYPE_CASHPOWER_IKCN:
            case TOKEN_TYPE_KCN:
                System.out.println("EncryptionSock - generateCSEToken : Token type not supported.");
                break;

            default:
                System.out.println("EncryptionSock - generateCSEToken : Unknown transfer number request type.");
                return false;
            }

        requestMessage = (tokenRequest.tokenClass + tokenRequest.tokenID + "0x0A");
        if (requestMessage.length() == 100) {
            sendRequestMessage(requestMessage);
            readResponseMessage();
            if (responseMessage.charAt(6)!= 'N') {
                System.out.println("EncryptionSocket-generateCSEToken : Invalid CSE response.");
                isErrorCritical = true;
                return false;
            }
        }

        return true;
    }



    //******************************************************************************************************************
    //
    //          Name    : getSTSKeyRegister ()
    //          Description : This method reads the key register map and returns the key register of the specified
    //                      : supply group.
    //
    //******************************************************************************************************************
    public boolean getSTSKeyRegister( String supplyGroup) {
        int keyReg = 0;
        int sgc = Integer.parseInt(supplyGroup);
        EncryptionTokenRequest tokenRequest = null;

        if (registerMap != null) {
            keyReg = registerMap.get(sgc);
        }
        tokenRequest.keyRegister = keyReg;

        return true;
    }



    //******************************************************************************************************************
    //
    //          Name    : testAndInitServer ()
    //          Description : This method tests and initialises the servers (Hardware Security Modules)  - The server
    //                      : type is provided as an input arg.
    //
    //******************************************************************************************************************
    public void testAndInitServer( EncryptionServerTypes serverType, String requestMessage) throws IOException {

        switch (serverType){
            case STS:
                if (requestMessage.equals("STS?ID")) {
                    sendRequestMessage(requestMessage);
                    readResponseMessage();
                    if (responseMessage.length() < 6){
                        System.out.println("EncryptionSocket - testAndInitServer : Invalid response");
                    }
                }
                break;
            case TMT:
                if (requestMessage.equals("TMT?ID")) {
                    sendRequestMessage(requestMessage);
                    readResponseMessage();
                    if (responseMessage.length() < 6){
                        System.out.println("EncryptionSocket - testAndInitServer : Invalid response");
                    }
                }
                break;
            case CTS:
                if (requestMessage.equals("CTS?ID")) {
                    sendRequestMessage(requestMessage);
                    readResponseMessage();
                    if (responseMessage.length() < 6){
                        System.out.println("EncryptionSocket - testAndInitServer : Invalid response");
                    }
                }
                break;
            default:
                System.out.println("EncryptionSocket-testAndInitSever : Server type / request not supported.");
                break;

        }

    }



    //******************************************************************************************************************
    //
    //          Name    : decryptToken ()
    //          Description : This method decrypts the token (encrypted using HSM encryption algorithm). The output is
    //                      : the actual token needed for consumption.
    //
    //******************************************************************************************************************
    public boolean decryptToken(String tokenStr, String meterNum, String supplyGroup, short tariffIndex, short keyReg, short keyRev,
        char tokenClass, int tokenId, long transferAmnt) throws IOException {
        char luhnChar;
        char stopString;
        char [] PAN = new char[20];
        char [] token = new char[21];
        short convertedTransferAmount;
        char [] tokenIdCheckStr = new char[7];
        char [] tokenDataCheckStr = new char[5];

        PAN = String.format("%s%s", STS_ELECTRICITY_ISOBIN_NO, meterNum).toCharArray();
        luhnChar = algorithm.checkLuhn(PAN,17);
        if (luhnChar == 'Z') {
            System.out.println("EncryptionSocket - decryptToken : PAN Luhn check failed.");
            return false;
        }

        PAN[17] = luhnChar;
        PAN[18] = ' ';
        PAN[19] = 0;

        requestMessage = ""; //reset the request string
        token = tokenStr.toCharArray();
        token[20] = 0;
        requestMessage = String.format("%s%s%02d%s%02d%d%s%s%s%d",
                         VERIFY_STS_CREDIT_TOKEN_REQ,
                         PAN,
                         supplyGroup,
                         tariffIndex,
                         keyRev,
                         KEY_EXPIRY_NUMBER,
                         token,
                           0
                );

        long crc = algorithm.addCRCToMessage(requestMessage);
        requestMessage += crc;

        if (requestMessage.length() > 0 && requestMessage.length() < 50 ) {
            sendRequestMessage(requestMessage);
            readResponseMessage();
            if (!validateSTSResponse(VERIFY_STS_CREDIT_TOKEN_RESP_LENGTH, VERIFY_STS_CREDIT_TOKEN_RESP)) {
                System.out.println("EncryptionSocket - decryptToken : Invalid response");
                return false;
            }

        }

        return true;
    }



    //******************************************************************************************************************
    //
    //          Name    : validateSTSResponse ()
    //          Description : This method validates the STS response message from HSM (server) - We expect the message
    //                      : to have some predefined length and response tag.
    //
    //******************************************************************************************************************
    private boolean validateSTSResponse(long expectedLength, String expectedResp) {
        if (!responseMessage.substring(5, responseMessage.length()).equals(expectedResp)) {
            System.out.println("EncryptionSocket - decryptToken : Invalid STS response");
            isErrorCritical = true;
            return false;
        }

        if (responseMessage.length() < expectedLength) {
            System.out.println("EncryptionSocket - decryptToken : Response too short");
            isErrorCritical = true;
            return false;
        }

        if (!responseMessage.substring(2, 5).equals("00")) {
            System.out.println("EncryptionSocket - decryptToken : Invalid STS response");
            isErrorCritical = true;
            return false;
        }
        return true;
    }



    //******************************************************************************************************************
    //
    //          Name    : convertResponseToSEQToken ()
    //          Description : This method converts the 8 byte encryption card response to a 20-digits token. This sequence
    //                      : is desired when processing the response from the encryption card.
    //
    //******************************************************************************************************************
    public boolean convertResponseToSEQToken(String token) {
        String scratchString = null;
        int i;

        for (i=1; i<7; i++) {
            scratchString += "0x0F";
            scratchString += "001";
        }

        return false;
    }



    private boolean convertResponseToPTSToken(String tempToken) {
        return true;
    }



    private void convertResponseToToken(String responseMessage) {
    }


    private int convertTransferAmountBack(int tokenData) {
        return tokenData;
    }



    private int convertTransferAmount(int i) {
        return i;
    }



}

