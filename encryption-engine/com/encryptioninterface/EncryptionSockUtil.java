package com.encryptioninterface;
//**********************************************************************************************************************
//
//          File    : EncryptionPortUtil.java
//          Class   : EncryptionPortUtil
//          Author  : Wiseman Abonile Sicuku
//          Date    : 2020-01-20
//          Description : This interface defines all STS and Non-STS utility (Water, Gas, Electricity) vending commands.
//                      : All the requests and Responds strings are defined according to STS-API Specification.
// W.A Sicuku
//**********************************************************************************************************************

public interface EncryptionSockUtil {
    // Requests and Response
    //------------------------------------------------------------------------------------------------------------------
    public static final String CREDIT_UPDATE_KEY			        = "A14F4C2B89BC14EA\0";
    public static final int MAX_ERROR_LENGTH				        =  40;
    public static final String STS_ELECTRICITY_ISOBIN_NO	        = "600727\0";
    public static final String STS_WATER_ISOBIN_NO			        = "603368\0";
    public static final String KEY_EXPIRY_NUMBER			        = "FF\0";
    public static final int CRC_LENGTH					        	=   5;
    public static final int SUBCLASS_MASK				            =  0x0F;

    public static final String GET_STS_CREDIT_STATUS		        = "SM?TS\0";
    public static final String GET_STS_CREDIT_STATUS_RESP	        = "SM!TS\0";
    public static final int GET_STS_CREDIT_STATUS_REQ_LENGTH		=   5;
    public static final int GET_STS_CREDIT_STATUS_RESP_LENGTH		=  18;

    public static final String UPDATE_STS_CREDIT					= "SM?TU\0";
    public static final String UPDATE_STS_CREDIT_RESP				= "SM!TU\0";
    public static final int UPDATE_STS_CREDIT_REQ_LENGTH			=  23;
    public static final int UPDATE_STS_CREDIT_RESP_LENGTH			=  18;

    public static final String GET_STS_MODULE_ID					= "SM?ID\0";
    public static final String GET_STS_MODULE_ID_RESP				= "SM!ID\0";
    public static final int GET_STS_MODULE_ID_REQ_LENGTH			=   5;
    public static final int GET_STS_MODULE_ID_RESP_LENGTH			=  44;

    public static final String INSERT_UPDATE_KEY_REQ				= "SM?IK\0";
    public static final String INSERT_UPDATE_KEY_RESP				= "SM!IK\0";
    public static final int INSERT_UPDATE_KEY_REQ_LENGTH			=  25;
    public static final int INSERT_UPDATE_KEY_RESP_LENGTH			=  28;

    public static final String GENERATE_STS_CREDIT_TOKEN_REQ		= "SM?TC\0";
    public static final String GENERATE_STS_CREDIT_TOKEN_RESP		= "SM!TC\0";
    public static final int GENERATE_STS_CREDIT_TOKEN_REQ_LENGTH	=  49;
    public static final int GENERATE_STS_CREDIT_TOKEN_RESP_LENGTH	=  49;

    public static final String GENERATE_STS_MAN_REQ					= "SM?TM\0";
    public static final String GENERATE_STS_MAN_RESP				= "SM!TM\0";
    public static final int GENERATE_STS_MAN_REQ_LENGTH				=  49;
    public static final int GENERATE_STS_MAN_RESP_LENGTH			=  49;

    public static final String GENERATE_STS_KEY_CHANGE_REQ			= "SM?TK\0";
    public static final String GENERATE_STS_KEY_CHANGE_RESP			= "SM!TK\0";
    public static final int GENERATE_STS_KEY_CHANGE_REQ_LENGTH		=  50;
    public static final int GENERATE_STS_KEY_CHANGE_RESP_LENGTH		=  86;

    public static final String VERIFY_STS_CREDIT_TOKEN_REQ			= "SM?TV\0";
    public static final String VERIFY_STS_CREDIT_TOKEN_RESP			= "SM!TV\0";
    public static final int VERIFY_STS_CREDIT_TOKEN_REQ_LENGTH		=  57;
    public static final int VERIFY_STS_CREDIT_TOKEN_RESP_LENGTH		=  25;

    public static final String GET_REGISTER_INFO_REQ				= "SM?GS\0";
    public static final String GET_REGISTER_INFO_RESP				= "SM!GS\0";
    public static final int GET_REGISTER_INFO_REQ_LENGTH			=   7;
    public static final int GET_REGISTER_INFO_RESP_LENGTH			=  29;
    public static final String  GET_REGISTER_INFO_REG_EMPTY_RESP	= "SM!GS04\0";

    public static final String  LOAD_KEY_REQ						= "SM?LK\0";
    public static final String  LOAD_KEY_RESP						= "SM!LK\0";
    public static final int LOAD_KEY_REQ_LENGTH						=  28;
    public static final int LOAD_KEY_RESP_LENGTH					=  23;

    public static final String  CLEAR_KEY_REQ					    = "SM?CK\0";
    public static final String  CLEAR_KEY_RESP					    = "SM!CK\0";
    public static final int CLEAR_KEY_REQ_LENGTH					=   7;
    public static final int CLEAR_KEY_RESP_LENGTH					=   7;

    public static final int PTS_SERIAL_NO_ECHO_REQ_LENGTH			=  10;
    public static final int PTS_SERIAL_NO_ECHO_RESP_LENGTH			=  10;

    //-----Protocol
    public static final int STS_ALGORITHM                           =  0x07;
    public static final int STS_CLASS_MAN_SET_DISP_KEY_A		    =  0x23;
    public static final int STS_CLASS_CREDIT_TRANSFER_ELEC          =  0x00;
    public static final int STS_CLASS_CREDIT_TRANSFER_WATER         =  0x01;
    public static final int STS_CLASS_CREDIT_TRANSFER_GAS           =  0x02;
    public static final int STS_CLASS_MAN_SET_MAX_PWR_LOAD			=  0x20;
    public static final int STS_CLASS_MAN_CLEAR_CREDIT				=  0x21;
    public static final int WATER_PREPAYMENT                        =  0x02;
    public static final int STS_CLASS_MAN_CLEAR_TAMPER_CONDITION    =  0x25;
    public static final int GAS_PREPAYMENT                          =  0x03;
    public static final int PLESSEY_ALGORITHM                       =  0x04;
    public static final int PLESSEY_CLASS_MAN_SET_CURRENT_LIMIT     =  0x30;
    public static final int PLESSEY_CLASS_MAN_CLEAR_REVERSE_FLAG    =  0x31;
    public static final int METER_TYPE_PTS_THREE_PHASE              =  0x32;
    public static final int METER_TYPE_PTS_SINGLE_PHASE             =  0x33;
    public static final int STS_CLASS_CREDIT_TRANSFER_CURRENCY      =  0x34;
    public static final int STS_CLASS_MAN_SET_TARIFF_RATE           =  0x35;
    public static final int STS_CLASS_MAN_SET_DISP_KEY_B            =  0x36;
    public static final int STS_CLASS_MAN_SET_WATER_FACTOR          =  0x37;
    public static final int STS_CLASS_MAN_VERIFY_DISPENSER_KEY      =  0x38;

    public static final int STS_CLASS_CREDIT_TRANSFER_CONNECTION_TIME   = 0x40;
    public static final int PLESSEY_CLASS_MAN_DISPLAY_PREVIOUS_BALANCE  = 0x41;
    public static final int METER_TYPE_SEQUENCE_WHOLE_UNIT              = 0x42;
    public static final int METER_TYPE_SEQUENCE_TENTH_UNIT              = 0x43;
    public static final int STS_CLASS_MAN_SET_PHASE_PWR_UNBALANCE_LIMIT = 0x39;

    public static final int TOKEN_TYPE_CREDIT           = 0x50;
    public static final int TOKEN_TYPE_TAMPER_RESET     = 0x51;
    public static final int TOKEN_TYPE_SET_OVERLOAD     = 0x52;
    public static final int TOKEN_TYPE_CLEAR_CREDIT     = 0x53;
    public static final int TOKEN_TYPE_CLEAR_LOG        = 0x54;
    public static final int TOKEN_TYPE_SET_LED_LEVELS   = 0x55;
    public static final int TOKEN_TYPE_CASHPOWER_IKCN   = 0x56;
    public static final int TOKEN_TYPE_KCN              = 0x57;

    //----
    public static final int TELLUMAT_UNLOCK_SEQURITY_REQ_LENGTH		=  5;
    public static final int TELLUMAT_UNLOCK_SEQURITY_RESP_LENGTH	=  3;

    public static final int TELLUMAT_ENCRYPT_TOKEN_SEQUENCE_METER_RESP_LENGTH	 = 8;
    public static final int TELLUMAT_ENCRYPT_TOKEN_PTS_METER_RESP_LENGTH		 = 10;
    //----

}
