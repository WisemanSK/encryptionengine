package com.encryptioninterface;
//**********************************************************************************************************************
//
//          File    : EncryptionAlgorithm.java
//          Class   : EncryptionAlgorithm
//          Author  : Wiseman Abonile Sicuku
//          Date    : 2020-01-28
//          Description : This class defines all encryption algorithms needed by the Encryption Engine for data processing
//                      :,formatting and manipulating.
//
// W.A Sicuku
//**********************************************************************************************************************
import java.util.zip.CRC32;
import java.util.zip.Checksum;



public class EncryptionAlgorithm {



    //******************************************************************************************************************
    //
    //          Name    : integerToHex ()
    //          Description : This method converts an integer value into a hexadecimal string. The input is a decimal
    //                      : value and the output is an array of hexadecimal characters.
    //
    //******************************************************************************************************************
    public char[] integerToHex(int value) {
        char[] hexString = new char[100];
        char[] hexResult = new char[100];
        int i = 0;
        while(value !=0) {
            int remValue = 0;
            remValue = value % 16;
            if (remValue < 10){
                hexString[i] = (char)(remValue + 48);
                ++i;
            }else {
                hexString[i] = (char)(remValue + 55);
                ++i;
            }
            value = value/16;
        }

        int k = 0;
        while(k != i) {
            for (int j = i - 1; j >= 0; --j) {
                hexResult[k] = hexString[j];
            }
            ++k;
        }
        return hexResult;
    }



    //******************************************************************************************************************
    //
    //          Name    : byteSwap ()
    //          Description : This method swap the bytes of a given long integer. The input argument in a long integer
    //                      : and the result is the byte swiped long integer.
    //
    //******************************************************************************************************************
    public long byteSwap(long x) {
        x = (x & 0x0000FFFF) << 16 | (x & 0xFFFF0000) >> 16;
        x = (x & 0x00FF00FF) << 8  | (x & 0xFF00FF00) >>  8;
        return x;
    }



    //******************************************************************************************************************
    //
    //          Name    : addCRCToMessage ()
    //          Description : This method calculates the CRC-32 value of the input message string - the result is the
    //                      : long integer value.
    //
    //******************************************************************************************************************
    public long addCRCToMessage(String requestMessage) {
        byte[] bytes = requestMessage.getBytes();
        Checksum checksum = new CRC32();        // java.util.zip.CRC32
        checksum.update(bytes, 0, bytes.length);
        return checksum.getValue();
    }



    //******************************************************************************************************************
    //
    //          Name    : bitsSet ()
    //          Description : This method set the bites of the input characters.
    //
    //
    //******************************************************************************************************************
    public int bitsSet(char ch){
        int k, n = 0;
        for (k = 0; k < 8; ++k){
            n += (ch & 0x01);
            ch >>=1;
        }
        return n;
    }



    //******************************************************************************************************************
    //
    //          Name    : ASCIIToHex ()
    //          Description : This method converts ascii string to hexadecimal value.
    //
    //
    //******************************************************************************************************************
    public String ASCIIToHex(String msgString) {
        StringBuilder builder = new StringBuilder();
        char[] ch = msgString.toCharArray();

        for (char c : ch) {
            int i  = (int) c;
            builder.append(Integer.toHexString(i).toUpperCase());
        }
        return builder.toString();
    }



    //******************************************************************************************************************
    //
    //          Name    : checkLuhn ()
    //          Description : This method uses Luhn algorithm on ASCII numeric string to calculate and return ascii
    //                      : single check digit '0' ... '9'. If any ascii digits are non numeric, then return 'Z'.
    //
    //
    //******************************************************************************************************************
    public char checkLuhn(char[] charStr, int length) {
        int i;
        int val;
        int nSum = 0;
        boolean leadCharPairF = true;

        for (i = length - 1; i >= 0; --i) {
            if (charStr[i] < '0' || charStr[i] > '9')
                return 'Z';

            val = charStr[i] - '0';

            if (leadCharPairF) {
                val *= 2;
                if (val > 9)
                    nSum = nSum + 1 + (val % 10);
                else
                    nSum += val;
            } else {
                nSum += val;
            }

            leadCharPairF = !leadCharPairF;
        }
        val = 10 - (nSum % 10);

        if (val ==10)
            val = 0;

        return (char)(val + '0');
    }



    //******************************************************************************************************************
    //
    //          Name    : checkLuhn ()
    //          Description : This method uses the Luhn algorithm to calculate the to the error check calculation on the
    //                      : given number.
    //
    //
    //******************************************************************************************************************
    public long checkLuhn(String numString)
    {
        int nDigits = numString.length();

        int nSum = 0;
        boolean isSecond = false;
        for (int i = nDigits - 1; i >= 0; i--)
        {

            int d = numString.charAt(i) - '0';

            if (isSecond == true)
                d = d * 2;

            // We add two digits to handle
            // cases that make two digits
            // after doubling
            nSum += d / 10;
            nSum += d % 10;

            isSecond = !isSecond;
        }
        return (nSum % 10);
    }



    //******************************************************************************************************************
    //
    //          Name    : convertTransferAmount ()
    //          Description : Generates the transfer amount string - inputs is the number of units to be transferred and
    //                      : the output is a 16-bit number (14-bit mantissa and a 2-bit exponent).
    //
    //
    //******************************************************************************************************************
    public long convertTransferAmount(long amount) {
        int transferAmountExponent;
        long transferAmountMantissa;

        if (amount <= 16383) {
            transferAmountExponent = 0;
        } else {
            if (amount <= 180214) {
                transferAmountExponent = 1;
            } else {
                if (amount <= 1818524) {
                    transferAmountExponent = 2;
                } else {
                    transferAmountExponent = 3;
                }

            }
        }

        switch (transferAmountExponent) {
            case 0:
                transferAmountMantissa = amount;
                break;

            case 1:
                transferAmountMantissa = (amount - 16384)/10;
                if ((amount-16384)%10 > 0) {
                    transferAmountMantissa +=1;
                }
                break;

            case 2:
                transferAmountMantissa = (amount - 180224)/100;
                if ((amount - 180224)%100 > 0 ) {
                    transferAmountMantissa += 1;
                }
                break;

            case 3:
                transferAmountMantissa = (amount - 1818624)/1000;
                if ((amount - 1818624)%1000 > 0 ) {
                    transferAmountMantissa += 1;
                }

                break;
            default:
                throw new IllegalStateException("Unexpected value: " + transferAmountExponent);
        }
        return (transferAmountExponent << 14) | (int) transferAmountMantissa;
    }

}
