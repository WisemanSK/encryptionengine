package com.encryptioninterface;



public enum  EncryptionServerTypes {
    CTS,
    STS,
    TMT;
}
