package com.encryptionprotocol;

public class EncryptionTokenRequest {
    public int meterType;
    public String tokenID;
    public long keyRegister;
    public int algorithm = 0;
    public int tokenData = 0;
    public String supplyGroup;
    public String meterNumber;
    public int tokenClass = 0;
    public String tariffIndex;
    public String oldSupplyGroup;
    public String oldTariffIndex;
    public ProtocolHeader hdr;
}
