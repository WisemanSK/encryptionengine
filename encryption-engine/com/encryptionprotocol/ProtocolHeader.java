package com.encryptionprotocol;





public class ProtocolHeader {

    public int pkt_id;          // packet type (MUST remain first field)
    public short proto_vers;    // protocol version
    public int pkt_vers;        // packet version
    public short length;        // length of packet including header
    public int pos_id;          // point of sale client id
    public int seq_no;          // request sequence number, 255 rollover
    public int error_code;      // used in both request & response
    public int utility;         // Electricity, Water, Gas et.al see valid utility types above
    public int[] sign = new int[16]; // digital signature for packet authentication
}

